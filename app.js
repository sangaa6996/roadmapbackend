import express from "express";
import cors from 'cors';
import bodyParser from "body-parser";
import dotenv from 'dotenv';
import router from './routes/index.js'
import mongoose from "mongoose";
import Role from "./models/roleModel.js";
import passport from "passport";
import * as passportSetup from "./passport.js";
import cookieSession from "cookie-session";
dotenv.config();

var app = express();
const port = process.env.PORT ?? 4000;
app.use(
    cors(
        {
            origin: process.env.CLIENT_URL,
            methods: "GET,POST,PUT,DELETE",
            credentials: true,
            optionsSuccessStatus:200
        }
    )
);
app.use(express.json());
app.use(bodyParser.json({ limit: "50mb", extended: true }));
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));
dotenv.config();

app.use(
	cookieSession({
        secret: 'anything'
	})
);
app.use(passport.initialize());
app.use(passport.session());

const initial = () => {
    // The estimatedDocumentCount() function is quick as it estimates the number of documents in the MongoDB collection. It is used for large collections because this function uses collection metadata rather than scanning the entire collection.

    Role.estimatedDocumentCount()
        .then((count) => {
            if (count === 0) {
                new Role({
                    name: "user",
                }).save()
                    .then(() => console.log("add role user to collection"))
                    .catch(err => console.log(err));

                new Role({
                    name: "moderator",
                }).save()
                    .then(() => console.log("add role mod to collection"))
                    .catch(err => console.log(err));

                new Role({
                    name: "admin",
                }).save()
                    .then(() => console.log("add role admin to collection"))
                    .catch(err => console.log(err));
            }
        });
}

const connect = async () => {
    try {
        await mongoose.connect(process.env.MONGODB_URL);
        initial();
        console.log("Connected to MongoDB")
    }
    catch (err) {
        console.log(err);
    }
}

connect();

app.use("/api", router)

app.listen(port, () => {
    console.log(`Server running on port ${port}`);
})