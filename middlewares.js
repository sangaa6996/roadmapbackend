import express from "express";
const routes = express.Router();
routes.use((req, res, next) => {
    let found = false;

    const passList = ["/user/authenticate"];

    if (req.originalUrl.startsWith("/user") && !passList.includes(req.originalUrl)) {
        found = true;
        if (!verifyBearer(req.token) && !whiteListSessionId.includes(req.originalUrl)) {
            checkSessionIdCms(req.headers.authorization)
                .then((data) => {
                    if (checkRestrictUser(data) || whiteList.includes(req.originalUrl)) {
                        req.userInfo = data;
                    } else {
                        res.status(401).send({ success: false, error: "Bạn đã bị hạn chế chức năng này!" });
                    }
                })
                .catch((e) => {
                    let response = {
                        success: false,
                        status: 'EXPIRE_TOKEN',
                        error: "Phiên đăng nhập của bạn đã hết hạn!",
                        detail: e,
                    };
                    res.status(401).send(response);
                });
        }
    }
});

export default routes;
