import express from "express";
import roadmapRoutes from "./roadmapRoutes.js"
import skillRoutes from "./skillRoutes.js"
import userRoutes from "./userRoutes.js"
import authRoutes from './authRoutes.js'
import progressRoutes from './progressRoutes.js'

const router = express.Router();

router.use("/roadmap", roadmapRoutes);
router.use("/skill", skillRoutes);
router.use("/user", userRoutes);
router.use("/auth", authRoutes);
router.use("/progress", progressRoutes)

export default router;