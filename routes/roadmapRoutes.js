import express from "express";
import Roadmap from '../models/roadmapModel.js'
import auth from "../utils/auth.js";
const routes = express.Router();

//Get all roadmap or get roadmap by name
routes.get("/", async (req, res) => {
    try {
        const params = req.query;
        const roadmaps = await Roadmap.find(params);
        res.status(200).send(roadmaps);
    } catch (error) {
        res.status(500).send({message: error});
    }
})

//create Roadmap
routes.post("/", auth, async (req,res) => {
    try {
        if(req.user.role !== 'admin'){
            throw 'User của bạn không có quyền để thực hiện thao tác này';
        }
        const roadmap = await Roadmap.create(req.body);
        res.status(200).send(roadmap)
    } catch (error) {
        res.status(500).send({message: error.message})
    }
})

//find first roadmap by name and update that roadmap 
routes.put("/", auth, async (req,res) => {
    try {
        const params = req.query;
        const oldRoadmap = await Roadmap.findOne(params);
        const newRoadmap = await Roadmap.findByIdAndUpdate(oldRoadmap._id, req.body);
        res.send(newRoadmap);
    } catch (error) {
        res.status(500).send({message: error.message})
    }
})

export default routes;
