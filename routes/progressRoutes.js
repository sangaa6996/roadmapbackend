import express from "express";
import Roadmap from '../models/roadmapModel.js'
import auth from "../utils/auth.js";
import Progress from "../models/progressModel.js";
const routes = express.Router();

//Get all roadmap or get roadmap by name
routes.get("/", auth, async (req, res) => {
    try {
        const params = req.query;
        const progress = await Progress.findOne({
            userId: req.user._id.toString(),
            nameRoadmap: params.resourceId
        });

        res.status(200).send({
            done: progress.done || [],
            learning: progress.learning || [],
            skipped: progress.skipped || []
        });
    } catch (error) {
        res.status(500).send({ message: error });
    }
})

routes.post("/update", auth, async (req, res) => {
    try {
        let progress = await Progress.findOne({
            userId: req.user._id.toString(),
            nameRoadmap: req.body.resourceId
        })

        if (!progress) {
            progress = Progress({
                userId: req.user._id.toString(),
                nameRoadmap: req.body.resourceId
            })
        }
        let progressAfterUpdate = await progress.updateProgress(req.body.progress, req.body.topicId);
        res.status(200).send(progressAfterUpdate)
    } catch (error) {
        res.status(500).send({ message: error.message })
    }
})

export default routes;
