import express from "express";
import Skill from "../models/skillModel.js";
const routes = express.Router();

//Get all skill or get skill by value
routes.get("/", async (req, res) => {
    try {
        const params = req.query;
        const result = await Skill.find(params);
        res.status(200).send(result);
    } catch (error) {
        res.status(500).send({message: error});
    }
})

//create Skill
routes.post("/", async (req,res) => {
    try {
        const skills = await Skill.create(req.body);
        res.status(200).send(skills)
    } catch (error) {
        res.status(500).send({message: error.message})
    }
})

//find first skill by value and update that skill 
routes.put("/", async (req,res) => {
    try {
        const params = req.query;
        const oldRoadmap = await Skill.findOne(params);
        const newRoadmap = await Skill.findByIdAndUpdate(oldRoadmap._id, req.body);
        res.send(newRoadmap);
    } catch (error) {
        res.status(500).send({message: error.message})
    }
})

export default routes;
