import express from "express";
import passport from "passport";
import dotenv from 'dotenv';
dotenv.config();

const routes = express.Router()

routes.get("/login/success", (req, res) => {
    if (req.user) {
        res.status(200).send({
            status: "ok",
            message: "Successfully Logged In",
            user: req.user,
        });
    } else {
        res.status(403).send({ error: true, message: "Not Authorized" });
    }
});

routes.get("/login/failed", (req, res) => {
    res.status(401).send({
        error: true,
        message: "Log in failure",
    });
});

routes.get("/google", passport.authenticate("google", ["profile", "email"]));
routes.get("/github", passport.authenticate("github", ["profile", "email"]));

routes.get(
	"/google/callback",
	passport.authenticate("google", {
		successRedirect: `${process.env.CLIENT_URL}/login?provider=google`,
		failureRedirect: "/login/failed",
	})
);

routes.get(
	"/github/callback",
	passport.authenticate("github", {
		successRedirect: `${process.env.CLIENT_URL}/login?provider=github`,
		failureRedirect: "/login/failed",
	})
);

routes.get("/logout", (req, res) => {
    console.log("🚀 ~ file: googleAuthRoutes.js:47 ~ routes.get ~ res:", res)
    req.logout();
    res.redirect(process.env.CLIENT_URL);
});

export default routes;