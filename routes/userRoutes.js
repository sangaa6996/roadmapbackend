import express from "express";
import User from "../models/userModel.js";
import auth from "../utils/auth.js";
import Role from "../models/roleModel.js";
import Progress from "../models/progressModel.js";
import sendEmail from "../utils/sendEmail.js";

const routes = express.Router()

routes.post('/', async (req, res) => {
    // Create a new user
    try {
        const user = new User(req.body)
        user.verified = false;
        const role = await Role.findOne({ name: req.body.role });
        if (!role) {
            user.role = 'user';
        }
        await user.save()
        const token = await user.generateAuthToken()
        sendEmail(req.body.email, "Verify account", `http://127.0.0.1:3000/verify-account?code=${user._id}`)
        res.status(201).send({ user, token, status: "ok" })
    } catch (error) {
        res.status(400).send(error)
    }
})

routes.post('/login', async (req, res) => {
    //Login a registered user
    try {
        const { email, password } = req.body
        const user = await User.findByCredentials(email, password)
        if (!user) {
            return res.status(401).send({ error: 'Login failed! Check authentication credentials' })
        }
        const token = await user.generateAuthToken()
        res.status(201).send({ user, token })
    } catch (error) {
        res.status(400).send(error)
    }
})

routes.post('/me/logout', auth, async (req, res) => {
    // Log user out of the application
    try {
        req.user.tokens = req.user.tokens.filter((token) => {
            return token.token != req.token
        })
        await req.user.save()
        res.send()
    } catch (error) {
        res.status(500).send(error)
    }
})

routes.post('/me/logoutall', auth, async (req, res) => {
    // Log user out of all devices
    try {
        req.user.tokens.splice(0, req.user.tokens.length)
        await req.user.save()
        res.send()
    } catch (error) {
        res.status(500).send(error)
    }
})

routes.get('/get-progess', auth, async (req, res) => {
    try {
        const progress = Progress.find({ userId: req.user.id, progressName: req.body.name });
        res.send()
    } catch (error) {
        res.status(500).send(error)
    }
})

routes.post('/verify-account', async (req, res) => {
    try {
        const user = await User.findById(req.body.code);
        user.verified = true;
        const token = await user.generateAuthToken()
        res.status(201).send({ user, token })
    } catch (error) {
        res.status(500).send(error)
    }
})

routes.post('/send-verification-email', async (req, res) => {
    try {
        const user = await User.findOne({email:req.body.email});
        sendEmail(req.body.email, "Verify account", `http://127.0.0.1:3000/verify-account?code=${user._id}`)
        res.status(201).send({ user })
    } catch (error) {
        res.status(500).send(error)
    }
})

export default routes