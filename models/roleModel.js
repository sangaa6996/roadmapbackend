import mongoose from "mongoose";

const roleSchema = mongoose.Schema(
    {
        name: {
            type: String,
            require: [true, "Please enter a name role value"]
        },
    },
    {
        timestamps: true
    }
)

const Role = mongoose.model('role', roleSchema);

export default Role;