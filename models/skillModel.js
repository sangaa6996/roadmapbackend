import mongoose from "mongoose";

const skillSchema = mongoose.Schema(
    {
        value: {
            type: String,
            require: [true, "Please enter a skill value"]
        },
        content: {
            type: String,
        },
    },
    {
        timestamps: true
    }
)

const Skill = mongoose.model('skill', skillSchema);

export default Skill;