import mongoose from "mongoose";

const roadmapSchema = mongoose.Schema(
    {
        name: {
            type: String,
            require: [true, "Please enter a road map name"]
        },
        initialNodes: {
            type: Array,
        },
        initialEdges: {
            type: Array
        }
    },
    {
        timestamps: true
    }
)

const Roadmap = mongoose.model('roadmap', roadmapSchema);

export default Roadmap;