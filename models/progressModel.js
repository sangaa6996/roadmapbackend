import mongoose from "mongoose";

const progressSchema = mongoose.Schema(
    {
        nameRoadmap: {
            type: String,
            require: true
        },
        userId: {
            type: String,
            require: true
        },
        done: {
            type: Array,
        },
        learning: {
            type: Array,
        },
        skipped: {
            type: Array
        }
    }
)

progressSchema.methods.updateProgress = async function (progressType, topicId) {
    const progress = this
    switch (progressType) {
        case 'done':
            progress[progressType].push(topicId);
            progress['learning'] = progress['learning'].filter(item => item !== topicId);
            progress['skipped'] = progress['skipped'].filter(item => item !== topicId);
            break;
        case 'learning':
            progress[progressType].push(topicId);
            progress['done'] = progress['done'].filter(item => item !== topicId);
            progress['skipped'] = progress['skipped'].filter(item => item !== topicId);
            break;
        case 'skipped':
            progress[progressType].push(topicId);
            progress['learning'] = progress['learning'].filter(item => item !== topicId);
            progress['done'] = progress['done'].filter(item => item !== topicId);
            break;

        default:
            progress['learning'] = progress['learning'].filter(item => item !== topicId);
            progress['done'] = progress['done'].filter(item => item !== topicId);
            progress['skipped'] = progress['skipped'].filter(item => item !== topicId);
            break;
    }
    await progress.save()
    return {
        learning: progress['learning'],
        done: progress['done'],
        skipped: progress['skipped'],
    }
}

const Progress = mongoose.model('progress', progressSchema);

export default Progress;