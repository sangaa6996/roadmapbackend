import { OAuth2Strategy } from "passport-google-oauth";
import { Strategy } from "passport-github2";
import passport from "passport";
import dotenv from 'dotenv';
import User from "./models/userModel.js";
dotenv.config();

passport.use(
    new OAuth2Strategy(
        {
            clientID: process.env.GOOGLE_CLIENT_ID,
            clientSecret: process.env.GOOGLE_SECRET_KEY,
            callbackURL: "/api/auth/google/callback",
            scope: ["profile", "email"],
        },
        async function (accessToken, refreshToken, profile, callback) {
            let user = await User.findOne({ email: profile.emails[0].value });
            if (!user) {
                user = User({
                    name: profile.displayName,
                    email: profile.emails[0].value,
                    verified: profile.emails[0].verified,
                    password: profile.id,
                    role: 'user'
                })
                await user.save();
            }
            profile.token = await user.generateAuthToken();
            callback(null, profile);
        }));

passport.use(
    new Strategy(
        {
            clientID: process.env.GITHUB_CLIENT_ID,
            clientSecret: process.env.GITHUB_SECRET_KEY,
            callbackURL: "/api/auth/github/callback",
            scope: ['profile','user:email'],
        },
        async function (accessToken, refreshToken, profile, callback) {
            let user = await User.findOne({ email: profile.emails[0].value });
            if (!user) {
                user = User({
                    name: profile.displayName,
                    email: profile.emails[0].value,
                    verified: profile.emails[0].verified,
                    password: profile.id,
                    role: 'user'
                })
                await user.save();
            }
            profile.token = await user.generateAuthToken();
            callback(null, profile);
        }));

passport.serializeUser((user, done) => {
    done(null, user);
});

passport.deserializeUser((user, done) => {
    done(null, user);
});